# README #

Projeto referente a disciplina de Programação Distribuída do curso de Tecnologia em Sistemas para Internet do IFPB usando o framework Flask.

Descrição do Projeto: https://sites.google.com/site/edemberg/Home/disciplinas/pod/projetos/rest

### Configuração ###

* Instalar python 2.7.9:  https://www.python.org/downloads/
* Instalar virtualenv: https://virtualenv.pypa.io/en/latest/installation.html
* git clone https://bitbucket.org/luishenriqueestrela/noticiasapp
* Digite o seguinte comando: 
                 * Windows: python virtualenv.py noticiasapp 
                                    flask/bin/pip install -r requirements.txt
                 * *nix: python virtualenv.py noticiasapp
                           flask/bin/pip install -r requirements.txt
* execute o comando python run_server.py
* execute o comando python client.py