from utils import dict_to_xml

import xmltodict
import requests
import json

class Error404(Exception):
    pass

def list_news():
    response = requests.get('http://localhost:5000/news')
    if response.status_code == 404: raise Error404('404: Resource not found')
    print "Status: %s" % (response.status_code)
    return response.json()
    

def get_news(news_id):
    response = requests.get('http://localhost:5000/news/' + news_id)
    if response.status_code == 404: raise Error404('404: Resource not found')
    print "Status: %s" % (response.status_code)
    return response.text
    

def create_news(title, author, content):
    xml_response = "<news><title>%s</title><author>%s</author><content>%s</content></news>" % (title, author, content)
    headers = {'content-type': 'text/xml'}
    response = requests.post('http://localhost:5000/news', data=xml_response, headers=headers)
    if response.status_code == 404: raise Error404('404: Resource not found')
    print "Status: %s" % (response.status_code)
    data = xmltodict.parse(response.text)
    return data

def update_news(id, title):
    payload = {'title': title}
    print payload
    headers = {'content-type': 'application/json'}
    print headers
    print 'http://localhost:5000/news/' + id
    response = requests.put('http://localhost:5000/news/' + id, data=json.dumps(payload), headers=headers)
    if response.status_code == 404: raise Error404('404: Resource not found')
    print "Status: %s" % (response.status_code)
    return response.json()

def delete_news(id):
    response = requests.delete('http://localhost:5000/news/' + news_id)
    if response.status_code == 404: raise Error404('404:Resource not found')
    return response.text

if __name__ == '__main__':
    while 1:
        op = raw_input("""
        Choose an action:
        0 - List news
        1 - Get News
        2 - Create News
        3 - Update News
        4 - Delete News
        """)
        if op == '1':
            try:
                data = get_news(raw_input('Choose the id of the news: '))
            except Error404, e:
                print e.message
            else:
                print data 
        elif op == '2':
            args = (raw_input('%s' % s) for s in ['Title: ', 'Author: ', 'Content: '])
            try:
                data = create_news(*args)
            except Error404, e:
                print e.message
            else:
                print """ 
        News create successfully:
        Title: %s
        Author: %s
        Date publish: %s
        Content: %s
                """ % tuple(data['news'][key] for key in reversed(data['news'].keys()))
        elif op == '3':
            args = (raw_input('%s' % s) for s in ['News Id: ', 'New title: '])
            try:
               data = update_news(*args) 
            except Error404, e:
                print e.message
            else:
                print """ 
        News update successfully:
        Author: %s
        Title: %s
        Author: %s
        Date publish: %s
        Content: %s
                """ % tuple(data[key] for key in reversed(data.keys()))
        elif op == '4':
            news_id = raw_input('Choose a news id: ')
            try:
                data = delete_news(news_id)
            except Error404, e:
                print e.message
            else:
                print "News deleted successfully."
                print data
        elif op == '0':
            try:
                news_list = list_news()
            except Error404, e:
                print e.message
            else:
                print news_list
