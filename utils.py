from xml.etree.ElementTree import Element, SubElement

def dict_to_xml(tag, o):

    elem = Element(tag)
    if isinstance(o, dict):
        for key, val in o.iteritems():
                elem.append(dict_to_xml(key, val))
    elif type(o) in (list, set, tuple):
        for item in o:
            elem.append(dict_to_xml("item", item))
    else:
        elem.text = unicode(o)

    return elem

