from flask import Flask, jsonify, abort, request, Response
from flask.ext.sqlalchemy import SQLAlchemy
from xml.etree.ElementTree import Element, SubElement, tostring
from utils import dict_to_xml

import os
import datetime
import xmltodict


#Config
basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'noticias.sqlite')
db = SQLAlchemy(app)

#Models
class News(db.Model):
    __tablename__ = 'news'
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(64))
    title = db.Column(db.String(80), unique=True)
    date = db.Column(db.DateTime, default=datetime.datetime.now())
    content = db.Column(db.UnicodeText)

    def __init__(self, author, title, content):
        self.author = author
        self.title = title
        self.content = content
    
    def __repr__(self):
        return self.title

    def to_xml(self):
        top = Element('news')

        title = SubElement(top, 'title')
        title.text = self.title

        author = SubElement(top, 'author')
        author.text = self.author

        date = SubElement(top, 'date')
        date.text = str(self.date)

        content = SubElement(top, 'content')
        content.text = self.content

        return tostring(top)



#Controllers
@app.route('/news', methods=['GET'])
def list_news():
    news_all = [dict(id=news.id, title=news.title) for news in News.query.all()]
    return jsonify(items=news_all)

@app.route('/news/<int:news_id>', methods=['GET'])
def get_news(news_id):
    news = News.query.get_or_404(news_id)
    attrs = """ 
    <p>Id: %s</p>
    <p>Title: %s</p>
    <p>Author: %s</p>
    <p>Date: %s</p>
    <p>Content: %s</p>
    """ % (news.id,
           news.title,
           news.author,
           news.date,
           news.content
          )
    return  Response(attrs, mimetype='text/html')

@app.route('/news', methods=['POST'])
def create_news():
    data = xmltodict.parse(request.data) if request.data else abort(400)
    news = News(data['news']['author'], data['news']['title'], data['news']['content'])
    db.session.add(news)
    db.session.commit()
    return  Response(news.to_xml(), mimetype='text/xml'), 201

@app.route('/news/<int:news_id>', methods=['PUT'])
def update_news(news_id):
    news = News.query.get_or_404(news_id)
    if 'title' in request.json and type(request.json['title']) != unicode:
        abort(400)
    news.title = request.json['title']
    db.session.commit()
    return  jsonify(id=news.id,
                    title=news.title,
                    author=news.author,
                    date=news.date,
                    content=news.content
                    )

@app.route('/news/<int:news_id>', methods=['DELETE'])
def delete_news(news_id):
    news = News.query.get_or_404(news_id)
    db.session.delete(news)
    db.session.commit()        
    return news.to_xml() 

if __name__ == '__main__':
    #db.create_all()
    app.run(debug=True)

